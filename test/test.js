const { assert } = require('chai');
const { newUser } = require('../index.js');

//describe gives structure to your test suite
describe('Test newUser Object', () => {

	//unit test
	//it() has two parameters, first is a description of what the test should do, and the second is an anonymous function
	
	//.equal = testing for equality ==

	it('Assert newUser type is object', () => {

		assert.equal(typeof(newUser),'object')

	});

	//.notEqual = testing for inequality !=

	it('Assert newUser.email is not undefined',() => {

		assert.notEqual(typeof(newUser.email),undefined)
	});

	//isAtLeast = testing for greater than or equal to >=
	
	it('Assert that the number of characters in newUser.password is at least 16', () => {

		assert.isAtLeast(newUser.password.length,16)
	});

	//Activity

	it('Assert newUser.firstName is string',() => {

		assert.equal(typeof(newUser.firstName),'string')
	});

	it('Assert newUser.lastName is string',() => {

		assert.equal(typeof(newUser.lastName),'string')
	});

	it('Assert newUser.firstName is not undefined',() => {

		assert.notEqual(typeof(newUser.firstName),undefined)
	});

	it('Assert newUser.lastName is not undefined',() => {

		assert.notEqual(typeof(newUser.lastName),undefined)
	});

	it('Assert value of age is at least 18',() => {

		assert.isAtLeast(newUser.age,18)
	});

	it('Assert newUser.age is number', () => {

		assert.equal(typeof(newUser.age),'number')
	});

	it('Assert newUser.contactNumber is string', () => {

		assert.equal(typeof(newUser.contactNumber),'string')
	});

	it('Assert that the number of characters in newUser.contactNumber is at least 11', () => {

		assert.isAtLeast(newUser.contactNumber.length,11)
	});

	it('Assert newUser.batchNumber is number',() =>{

		assert.equal(typeof(newUser.batchNumber),'number')
	});

	it('Assert newUser.batchNumber is not undefined', () => {

		assert.notEqual(typeof(newUser.batchNumber),undefined)
	});

});